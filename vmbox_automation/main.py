from pickle import TRUE
import vm_tools
import cli_tools

print("vmbox_automation v1.0")
while TRUE:
    cli = input()
    commands = {
        "list": vm_tools.list_machines,
        "clear": cli_tools.clear,
        "create": cli_tools.create()
    }
    try:
        commands.get(cli)()
    except (KeyError, TypeError):
        if(cli == "exit"):
            break
        print("command: {} not found".format(cli))

# tools.list_machines()
# tools.get_session_state()
# tools.get_vm_state("Ubuntu_default_1649167321696_64212")
# input("type exit to close\r\n")
