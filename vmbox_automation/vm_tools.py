from ast import Str
import virtualbox

# VARIABLES
vbox = virtualbox.VirtualBox()
session = virtualbox.Session()

# FUNCTIONS
def list_machines():
    vms = [m.name for m in vbox.machines]
    for vm in vms:
        print(vm + "\t" + get_vm_state(vm))

def get_session_state():
    print(session.state)

def get_vm_state(vm_name) -> Str:
    machine = vbox.find_machine(vm_name)
    return machine.state.__str__()

def create_vm():
    vbox.create_machine()