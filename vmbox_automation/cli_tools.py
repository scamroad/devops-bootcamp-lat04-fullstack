import os
import platform
import vm_tools

# VARIABLES
my_os = platform.system()

# FUNCTIONS
def clear():
    if(my_os == "Linux"):
        os.system("clear")
    if(my_os == "Windows"):
        os.system("cls")

def create():
    vm_tools.create_vm()